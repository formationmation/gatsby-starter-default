FROM node AS build

WORKDIR /gatsby

COPY . .

RUN npm install -g gatsby-cli 

RUN npm install

RUN gatsby build



FROM nginx AS deploy

COPY --from=build /gatsby/public /usr/share/nginx/html
